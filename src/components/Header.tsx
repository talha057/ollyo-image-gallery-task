import { useImageStore, useSelectForDelete } from "@/stores/image-store";

const Title = () => (
	<h1 className="text-3xl font-bold text-gray-900 dark:text-gray-100 py-2">
		Gallery
	</h1>
);

const DeletionHeader = () => {
	const removeImages = useImageStore((state) => state.removeImages);
	const { url, clearUrl } = useSelectForDelete();
	const handleDelete = () => {
		removeImages(url);
		clearUrl();
	};

	return (
		<div className="flex items-center justify-between">
			<h1 className="text-3xl font-bold text-gray-900 dark:text-gray-100 py-2">
				<input
					value=""
					defaultChecked
					type="checkbox"
					className="mr-4 w-6 h-6 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600 peer"
				/>
				<span className="font-semibold">{url.length} Items Selected</span>
			</h1>
			<button
				type="button"
				onClick={handleDelete}
				className="px-4 py-2 text-sm font-medium text-white bg-red-500 rounded-lg hover:bg-red-600 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
			>
				Delete
			</button>
		</div>
	);
};

const Header = () => {
	const { url } = useSelectForDelete();

	return (
		<div className="border-b border-b-gray-300 dark:border-b-gray-100">
			{url.length < 1 ? <Title /> : <DeletionHeader />}
		</div>
	);
};

export default Header;
