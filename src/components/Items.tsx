import { useImageStore, useSelectForDelete } from "@/stores/image-store";
import { useEffect, useId, useState } from "react";

type ItemsProps = {
	url: string;
	index: number;
	title?: string;
	isFeatured?: boolean;
};

const Items = ({ url, index, title = "", isFeatured = false }: ItemsProps) => {
	const id = useId();
	const [selected, setSelected] = useState(false);

	const sortImages = useImageStore((state) => state.sortImages);
	const { addUrl, removeUrl } = useSelectForDelete();

	useEffect(() => {
		if (selected) addUrl(url);
		else removeUrl(url);
	}, [addUrl, removeUrl, selected, url]);

	useEffect(() => {
		setSelected(false);
	}, [url]);

	const handleDragOver = (e: React.DragEvent<HTMLDivElement>) =>
		e.preventDefault();

	const handleOnDragStart = (e: React.DragEvent<HTMLDivElement>) =>
		e.dataTransfer.setData("text/plain", index.toString());

	const handleOnDrop = (e: React.DragEvent<HTMLDivElement>) => {
		e.preventDefault();
		const from = Number(e.dataTransfer.getData("text/plain"));
		const to = index;
		sortImages(from, to);
	};

	return (
		<div
			className={`border border-slate-100 rounded-lg ${
				isFeatured ? "col-span-2 row-span-2" : ""
			}`}
			onDragOver={handleDragOver}
			onDrop={handleOnDrop}
		>
			<div
				className="relative group rounded-lg"
				draggable
				onDragStart={handleOnDragStart}
			>
				<img className="h-auto max-w-full rounded-lg" src={url} alt={title} />
				<label
					htmlFor={id}
					className={`absolute inset-0 z-10 bg-black rounded-lg transition-all ${
						selected
							? "bg-opacity-25"
							: "opacity-0 group-hover:opacity-75 group-hover:bg-opacity-50"
					}`}
				>
					<input
						id={id}
						value=""
						type="checkbox"
						onChange={(e) => setSelected(e.target.checked)}
						className="absolute top-0 left-0 m-4 w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
					/>
				</label>
			</div>
		</div>
	);
};

export default Items;
