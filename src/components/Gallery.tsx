import { useImageStore } from "@/stores/image-store";
import Input from "@components/Input";
import Items from "@components/Items";

const Gallery = () => {
	const items = useImageStore((state) => state.items);

	return (
		<div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-5 gap-2">
			{items.map((item, index) => (
				<Items
					key={index}
					url={item}
					index={index}
					title=""
					isFeatured={index === 0}
				/>
			))}

			{/* Upload Component */}
			<Input />
		</div>
	);
};

export default Gallery;
