import Gallery from "@components/Gallery";
import Header from "@components/Header";

const App = () => {
	return (
		<div className="max-w-5xl mx-auto">
			<div className="space-y-2 bg-white shadow-md p-2 rounded-md">
				<Header />
				<Gallery />
			</div>
		</div>
	);
};

export default App;
