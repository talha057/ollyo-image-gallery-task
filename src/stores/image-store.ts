import { create } from "zustand";

// const urls = [
// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-1.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-2.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-3.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-4.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-5.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-6.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-7.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-8.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-9.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-10.jpg",

// 	"https://flowbite.s3.amazonaws.com/docs/gallery/square/image-11.jpg",
// ];

const urls = [
	"/images/image-11.jpeg",
	"/images/image-1.webp",
	"/images/image-2.webp",
	"/images/image-3.webp",
	"/images/image-4.webp",
	"/images/image-5.webp",
	"/images/image-10.jpeg",
	"/images/image-6.webp",
	"/images/image-7.webp",
	"/images/image-8.webp",
	"/images/image-9.webp",
];

export type ImageStore = {
	items: string[];
	addImages: (images: string[]) => void;
	removeImages: (image: string[]) => void;
	sortImages: (draggedIndex: number, targetIndex: number) => void;
};

export const useImageStore = create<ImageStore>((set) => ({
	items: urls,
	addImages: (images: string[]) =>
		set((state) => ({ items: [...state.items, ...images] })),
	removeImages: (image: string[]) => {
		set((state) => ({
			items: state.items.filter((i) => !image.includes(i)),
		}));
	},
	sortImages: (draggedIndex: number, targetIndex: number) => {
		set((state) => {
			const items = [...state.items];
			const draggedItem = items[draggedIndex];
			items.splice(draggedIndex, 1);
			items.splice(targetIndex, 0, draggedItem);
			return { items };
		});
	},
}));

type SelectForDelete = {
	url: string[];
	addUrl: (url: string) => void;
	removeUrl: (url: string) => void;
	clearUrl: () => void;
};

export const useSelectForDelete = create<SelectForDelete>((set) => ({
	url: [],
	addUrl: (url: string) => set((state) => ({ url: [...state.url, url] })),
	removeUrl: (url: string) =>
		set((state) => ({ url: state.url.filter((u) => u !== url) })),
	clearUrl: () => set(() => ({ url: [] })),
}));
